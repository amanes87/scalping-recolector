package com.scalpingjedi.recolector.entidades;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.scalpingjedi.comun.entidades.dto.trade.TradeData;
import com.scalpingjedi.comun.entidades.trades.ResumenTrade;

/**
 * Clase no persistida para trabajar con el WebSocketTrades.
 * @author Adrià
 *
 */
public class Trades {

	private LocalDateTime horaCierre;

	private Double open;

	private Double high;

	private Double low;

	private Double close;

	// LONGS
	private List<Double> precioLong;

	private Double volumenLong;

	private Integer countLong;

	private Double volumenMayorLong;

	// SHORT
	private List<Double> precioShort;

	private Double volumenShort;

	private Integer countShort;

	private Double volumenMayorShort;

	public Trades(TradeData trade) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		this.horaCierre = LocalDateTime.parse(trade.getTimestamp(), formatter).plusMinutes(5);
		this.open = trade.getPrice();
		this.high = trade.getPrice();
		this.low = trade.getPrice();
		this.close = trade.getPrice();
		this.volumenLong = 0D;
		this.volumenShort = 0D;
		this.precioLong = new ArrayList<>();
		this.precioShort = new ArrayList<>();
		this.countLong = 0;
		this.countShort = 0;
		this.volumenMayorLong = 0D;
		this.volumenMayorShort = 0D;
	}

	public void addSell(TradeData trade) {
		addSellBuyComun(trade);
		this.volumenShort = this.volumenShort + trade.getSize().intValue();
		precioShort.add(trade.getPrice());
		this.countShort++;
		addVolumenMayorShort(trade.getSize());
	}

	public void addBuy(TradeData trade) {
		addSellBuyComun(trade);
		this.volumenLong = this.volumenLong + trade.getSize().intValue();
		precioLong.add(trade.getPrice());
		this.countLong++;
		addVolumenMayorLong(trade.getSize());
	}

	private void addVolumenMayorLong(BigDecimal size) {
		if (this.volumenMayorLong < size.doubleValue()) {
			this.volumenMayorLong = size.doubleValue();
		}
	}

	private void addVolumenMayorShort(BigDecimal size) {
		if (this.volumenMayorShort < size.doubleValue()) {
			this.volumenMayorShort = size.doubleValue();
		}
	}

	private void addSellBuyComun(TradeData trade) {
		if (trade.getPrice() > this.high) {
			this.high = trade.getPrice();
		}
		if (trade.getPrice() < this.low) {
			this.low = trade.getPrice();
		}
		this.close = trade.getPrice();
	}

	/**
	 * Método encargado de retornar la entidad ResumenTrade. Se utiliza justo antes de insertar un ResumenTrade al cierre de 5 minutos
	 * @return
	 */
	public ResumenTrade obtenerResumenTrades() {
		ResumenTrade trade = new ResumenTrade();
		trade.setOpen(new BigDecimal(this.open));
		trade.setHigh(new BigDecimal(this.high));
		trade.setLow(new BigDecimal(this.low));
		trade.setClose(new BigDecimal(this.close));
		trade.setFechaCierre(this.horaCierre);
		Double mediaLong = this.precioLong.stream().mapToDouble(Double::doubleValue).average().orElse(0d);
		trade.setPrecioLong(new BigDecimal(mediaLong));
		Double mediaShort = this.precioShort.stream().mapToDouble(Double::doubleValue).average().orElse(0d);
		trade.setPrecioShort(new BigDecimal(mediaShort));
		trade.setVolumenShort(new BigDecimal(this.volumenShort));
		trade.setVolumenLong(new BigDecimal(this.volumenLong));
		Double tamanoLong = (this.volumenMayorLong / 40000 * 3);
		trade.setTamanoLong(tamanoLong > 70 ? new BigDecimal(70) : new BigDecimal(tamanoLong));
		trade.setVolumenMayorLong(new BigDecimal(this.volumenMayorLong));
		Double tamanoShort = (this.volumenMayorShort / 40000 * 3);
		trade.setVolumenMayorShort(new BigDecimal(this.volumenMayorShort));
		trade.setTamanoShort(tamanoShort > 70 ? new BigDecimal(70) : new BigDecimal(tamanoShort));
		trade.setCountLong(this.countLong);
		trade.setCountShort(this.countShort);
		trade.setPrecioLiquidacion100Long(new BigDecimal((trade.getPrecioLong().doubleValue() * 0.9953)).setScale(1, RoundingMode.HALF_UP));
		trade.setPrecioLiquidacion50Long(new BigDecimal((trade.getPrecioLong().doubleValue() * 0.9853)).setScale(1, RoundingMode.HALF_UP));
		trade.setPrecioLiquidacion25Long(new BigDecimal((trade.getPrecioLong().doubleValue() * 0.968)).setScale(1, RoundingMode.HALF_UP));
		trade.setPrecioLiquidacion100Short(new BigDecimal((trade.getPrecioShort().doubleValue() * 1.0047)).setScale(1, RoundingMode.HALF_UP));
		trade.setPrecioLiquidacion50Short(new BigDecimal((trade.getPrecioShort().doubleValue() * 1.0147)).setScale(1, RoundingMode.HALF_UP));
		trade.setPrecioLiquidacion25Short(new BigDecimal((trade.getPrecioShort().doubleValue() * 1.0339)).setScale(1, RoundingMode.HALF_UP));
		return trade;
	}

}

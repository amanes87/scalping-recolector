package com.scalpingjedi.recolector.controller;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scalpingjedi.recolector.helper.DatosEnMemoria;
import com.scalpingjedi.recolector.servicios.bitmex.websockets.WebSocketLiquidaciones;
import com.scalpingjedi.recolector.servicios.bitmex.websockets.WebSocketTrades;
import com.scalpingjedi.recolector.servicios.bitmex.websockets.WebSocketVelas;

/**
 * Endpoint para gestionar los webSockets de la aplicación.
 * @author Adrià
 *
 */
@RestController
public class RecolectorController {
	
	@Autowired
	private DatosEnMemoria datosEnMemoria;
	
	@Autowired
	private WebSocketLiquidaciones wsLiquidaciones;
	
	@Autowired
	private WebSocketTrades wsTrades;
	
	@Autowired
	private WebSocketVelas wsVelas;
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@GetMapping(value = "/iniciar")
	public void inicializarHilos(HttpServletRequest request, HttpServletResponse response) {
		String mensaje = "REINICIO DE HILOS";
		log.info(mensaje);
		datosEnMemoria.getTelegramSv().notifyMessage(mensaje);
		inicializarHiloTrades(request, response);
		inicializarHiloVelas(request, response);
		inicializarHiloLiquidaciones(request, response);
		
	}
	
	/**
	 * Este metodo comprueba todos los hilos y los inicializa si están caidos
	 * @param request
	 * @param response
	 */
	@GetMapping(value = "/estado")
	public void estadoAplicacion(HttpServletRequest request, HttpServletResponse response) {
		String mensaje = "ESTADO APLICACIÓN";
		log.info(mensaje);
		if (!isEstadoTradesOpen()) {
			inicializarHiloTrades(request, response);
		}
		if (!isEstadoVelasOpen()) {
			inicializarHiloVelas(request, response);
		}
		if (!isEstadoLiquidacionesOpen()) {
			inicializarHiloLiquidaciones(request, response);
		}
	}
	
	////////////////////TRADES //////////////////// 
	@GetMapping(value = "/estado/trades")
	public void estadoTrades(HttpServletRequest request, HttpServletResponse response) {
		boolean isOpenTrades = isEstadoTradesOpen();
		if (isOpenTrades) {
			response.setStatus(HttpStatus.ACCEPTED.value());
		}else {
			response.setStatus(HttpStatus.GONE.value());
		}
	}
	
	/**
	 * Metodo que notifica el estado del socket de trades y retorna el estado
	 * @return
	 */
	private boolean isEstadoTradesOpen() {
		boolean isOpenTrades = datosEnMemoria.getSocketTrades().isOpen();
		String resultado = MessageFormat.format("ESTADO SOCKET TRADES {0}", isOpenTrades);
		log.info(resultado);
		datosEnMemoria.getTelegramSv().notifyMessage(resultado);
		return isOpenTrades;
	}
	
	@GetMapping(value = "/iniciar/trades")
	public void inicializarHiloTrades(HttpServletRequest request, HttpServletResponse response) {
		datosEnMemoria.getTelegramSv().notifyMessage("INICIALIZAR HILO TRADES");
		datosEnMemoria.getSocketTrades().close();
		datosEnMemoria.setSocketTrades(wsTrades.iniciar());
	}
	
	//////////////////// VELAS //////////////////// 
	@GetMapping(value = "/estado/velas")
	public void estadoVelas(HttpServletRequest request, HttpServletResponse response) {
		boolean isOpenVelas = isEstadoVelasOpen();
		if (isOpenVelas) {
			response.setStatus(HttpStatus.ACCEPTED.value());
		}else {
			response.setStatus(HttpStatus.GONE.value());
		}
	}
	
	/**
	 * Metodo que notifica el estado del socket de velas y retorna el estado
	 * @return
	 */
	private boolean isEstadoVelasOpen() {
		boolean isOpenVelas = datosEnMemoria.getSocketVelas().isOpen();
		String resultado = MessageFormat.format("ESTADO SOCKET VELAS {0}", isOpenVelas);
		log.info(resultado);
		datosEnMemoria.getTelegramSv().notifyMessage(resultado);
		return isOpenVelas;
	}
	
	@GetMapping(value = "/iniciar/velas")
	public void inicializarHiloVelas(HttpServletRequest request, HttpServletResponse response) {
		datosEnMemoria.getTelegramSv().notifyMessage("INICIALIZAR HILO VELAS");
		datosEnMemoria.getSocketVelas().close();
		datosEnMemoria.setSocketVelas(wsVelas.iniciar());
	}
	
	//////////////////// LIQUIDACIONES //////////////////// 
	@GetMapping(value = "/estado/liquidaciones")
	public void estadoLiquidaciones(HttpServletRequest request, HttpServletResponse response) {
		boolean isOpenLiquidaciones = isEstadoLiquidacionesOpen();
		if (isOpenLiquidaciones) {
			response.setStatus(HttpStatus.ACCEPTED.value());
		}else {
			response.setStatus(HttpStatus.GONE.value());
		}
	}
	
	/**
	 * Metodo que notifica el estado del socket de liquidaciones y retorna el estado
	 * @return
	 */
	private boolean isEstadoLiquidacionesOpen() {
		boolean isOpenLiquidaciones = datosEnMemoria.getSocketLiquidaciones().isOpen();
		String resultado = MessageFormat.format("ESTADO SOCKET LIQUIDACIONES {0}", isOpenLiquidaciones);
		log.info(resultado);
		datosEnMemoria.getTelegramSv().notifyMessage(resultado);
		return isOpenLiquidaciones;
	}
	
	
	@GetMapping(value = "/iniciar/liquidaciones")
	public void inicializarHiloLiquidaciones(HttpServletRequest request, HttpServletResponse response) {
		datosEnMemoria.getTelegramSv().notifyMessage("INICIALIZAR HILO LIQUIDACIONES");
		datosEnMemoria.getSocketLiquidaciones().close();
		datosEnMemoria.setSocketLiquidaciones(wsLiquidaciones.iniciar());
	}
}

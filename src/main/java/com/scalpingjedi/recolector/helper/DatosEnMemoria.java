package com.scalpingjedi.recolector.helper;

import org.java_websocket.WebSocket;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.telegram.service.TelegramSv;

import lombok.Getter;
import lombok.Setter;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DatosEnMemoria {
	
	@Getter
	private TelegramSv telegramSv;

	public void inicializarTelegram(String chatId, String url) {
		telegramSv = new TelegramSv(chatId, url);
	}
	
	@Getter
	@Setter
	private WebSocket socketTrades;
	
	@Getter
	@Setter
	private WebSocket socketVelas;
	
	@Getter
	@Setter
	private WebSocket socketLiquidaciones;

}

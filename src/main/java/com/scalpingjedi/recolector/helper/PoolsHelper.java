package com.scalpingjedi.recolector.helper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.scalpingjedi.comun.entidades.dto.trade.TradeData;

import lombok.Getter;
import lombok.Setter;

@Component
@Scope(value = "singleton")
public class PoolsHelper {

	// precio, volumen
	@Getter
	@Setter
	private static Map<Integer,BigDecimal> mapPools100x;
	
	@Getter
	@Setter
	private static Map<Integer,BigDecimal> mapPools50x;
	
	@Getter
	@Setter
	private static Map<Integer,BigDecimal> mapPools25x;
	
	@Getter
	@Setter
	private static Map<Integer,BigDecimal> mapPools10x;
	
	@Getter
	@Setter
	private static Map<Integer,BigDecimal> mapPools5x;
	
	@Getter
	@Setter
	private static Boolean poolGap;
	
	@Getter
	@Setter
	private static int id2;


	public static void inicializarPools() {
		mapPools100x =  Collections.synchronizedMap(new HashMap<Integer,BigDecimal>());
		mapPools50x =  Collections.synchronizedMap(new HashMap<Integer,BigDecimal>());
		mapPools25x =  Collections.synchronizedMap(new HashMap<Integer,BigDecimal>());
		mapPools10x =  Collections.synchronizedMap(new HashMap<Integer,BigDecimal>());
		mapPools5x =  Collections.synchronizedMap(new HashMap<Integer,BigDecimal>());
		id2 = 0;
		poolGap = Boolean.FALSE;
	}

	public static void acumularPools(TradeData tradeIteracion) {
		Double precio100x, precio50x, precio25x, precio10x, precio5x;
		if (tradeIteracion.getSide().equals("Buy")) {
			precio100x = tradeIteracion.getPrice() * 0.9953;
			precio50x = tradeIteracion.getPrice() * 0.9853;
			precio25x = tradeIteracion.getPrice() * 0.968;
			precio10x = tradeIteracion.getPrice() * 0.905;
			precio5x = tradeIteracion.getPrice() * 0.805;
		} else {
			precio100x = tradeIteracion.getPrice() * 1.0047;
			precio50x = tradeIteracion.getPrice() * 1.0147;
			precio25x = tradeIteracion.getPrice() * 1.0339;
			precio10x = tradeIteracion.getPrice() * 1.095;
			precio5x = tradeIteracion.getPrice() * 1.195;
		}

		Integer precio100xint = retornarNumeroAgrupado(precio100x);
		Integer precio50xint = retornarNumeroAgrupado(precio50x);
		Integer precio25xint = retornarNumeroAgrupado(precio25x);
		Integer precio10xint = retornarNumeroAgrupado(precio10x);
		Integer precio5xint = retornarNumeroAgrupado(precio5x);
		
		BigDecimal volumen = tradeIteracion.getSize();

		if (!mapPools100x.isEmpty() && mapPools100x.containsKey(precio100xint)) {
			mapPools100x.put(precio100xint, mapPools100x.get(precio100xint).add(volumen));
		}else {
			mapPools100x.put(precio100xint,volumen);
		}
		if (!mapPools50x.isEmpty() && mapPools50x.containsKey(precio50xint)) {
			mapPools50x.put(precio50xint, mapPools50x.get(precio50xint).add(volumen));
		}else {
			mapPools50x.put(precio50xint, volumen);
		}
		if (!mapPools25x.isEmpty() && mapPools25x.containsKey(precio25xint)) {
			mapPools25x.put(precio25xint, mapPools25x.get(precio25xint).add(volumen));
		}else {
			mapPools25x.put(precio25xint, volumen);
		}
		if (!mapPools10x.isEmpty() && mapPools10x.containsKey(precio10xint)) {
			mapPools10x.put(precio10xint, mapPools10x.get(precio10xint).add(volumen));
		}else {
			mapPools10x.put(precio10xint, volumen);
		}
		if (!mapPools5x.isEmpty() && mapPools5x.containsKey(precio5xint)) {
			mapPools5x.put(precio5xint, mapPools5x.get(precio5xint).add(volumen));
		}else {
			mapPools5x.put(precio5xint, volumen);
		}
	}

	/**
	 * Retorna el precio agrupado dependiendo de la agrupación para que la carga de datos sea más libiana
	 * @param precio
	 * @return
	 */
	public static Integer retornarNumeroAgrupado(Double precio) {
		BigDecimal precioRango = new BigDecimal(precio).divide(new BigDecimal(5), 0, RoundingMode.DOWN);
		return precioRango.multiply(new BigDecimal(5)).intValue();
	}

}

package com.scalpingjedi.recolector.helper;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.scalpingjedi.comun.entidades.Orden;
import com.scalpingjedi.comun.entidades.bitfinex.candle.Candle;
import com.scalpingjedi.comun.entidades.dto.trade.TradeData;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;

@Component
@Scope(value = "singleton")
public class OrdenesHelper {

	private static Orden ordenesAcumuladas;

	private static BigDecimal volumenAcumuladoBuy;
	private static BigDecimal precioVolumenAcumuladoBuy;

	private static BigDecimal volumenAcumuladoSell;

	private static BigDecimal precioVolumenAcumuladoSell;
	private static Integer cantidadBuy;
	private static Integer cantidadSell;
	
	private static Vela_005 velaEnMemoria;
	
	private static Candle lastCandle;

//	@Getter
//	@Setter
//	private static Map<Double, Double> mapPrecioVolumenesBuy;
//	
//	@Getter
//	@Setter
//	private static Map<Double, Double> mapPrecioVolumenesSell;


	public static synchronized Orden getOrdenesAcumuladas() {
		return ordenesAcumuladas;
	}

	public static synchronized void setOrdenesAcumuladas(Orden ordenesAcumuladas) {
		OrdenesHelper.ordenesAcumuladas = ordenesAcumuladas;
	}

	public static synchronized BigDecimal getVolumenAcumuladoBuy() {
		return volumenAcumuladoBuy;
	}

	public static synchronized void setVolumenAcumuladoBuy(BigDecimal volumenAcumuladoBuy) {
		OrdenesHelper.volumenAcumuladoBuy = volumenAcumuladoBuy;
	}

	public static synchronized BigDecimal getVolumenAcumuladoSell() {
		return volumenAcumuladoSell;
	}

	public static synchronized void setVolumenAcumuladoSell(BigDecimal volumenAcumuladoSell) {
		OrdenesHelper.volumenAcumuladoSell = volumenAcumuladoSell;
	}

	public static synchronized BigDecimal getPrecioVolumenAcumuladoSell() {
		return precioVolumenAcumuladoSell;
	}

	public static synchronized void setPrecioVolumenAcumuladoSell(BigDecimal precioVolumenAcumuladoSell) {
		OrdenesHelper.precioVolumenAcumuladoSell = precioVolumenAcumuladoSell;
	}

	public static synchronized BigDecimal getPrecioVolumenAcumuladoBuy() {
		return precioVolumenAcumuladoBuy;
	}

	public static synchronized void setPrecioVolumenAcumuladoBuy(BigDecimal precioVolumenAcumuladoBuy) {
		OrdenesHelper.precioVolumenAcumuladoBuy = precioVolumenAcumuladoBuy;
	}

	public static synchronized Integer getCantidadBuy() {
		return cantidadBuy;
	}

	public static synchronized void setCantidadBuy(Integer cantidadBuy) {
		OrdenesHelper.cantidadBuy = cantidadBuy;
	}

	public static synchronized Integer getCantidadSell() {
		return cantidadSell;
	}

	public static synchronized void setCantidadSell(Integer cantidadSell) {
		OrdenesHelper.cantidadSell = cantidadSell;
	}
	
	public static synchronized Vela_005 getVelaEnMemoria() {
		return velaEnMemoria;
	}
	
	public static synchronized void setVelaEnMemoria(Vela_005 vela) {
		OrdenesHelper.velaEnMemoria = vela;
	}
	
	public static synchronized Candle getLastCandle() {
		return lastCandle;
	}
	
	public static synchronized void setLastCandle(Candle candle) {
		OrdenesHelper.lastCandle = candle;
	}

	public static void inicializarAcumuladores() {
		volumenAcumuladoBuy = BigDecimal.ZERO;
		volumenAcumuladoSell = BigDecimal.ZERO;
		precioVolumenAcumuladoBuy = BigDecimal.ZERO;
		precioVolumenAcumuladoSell = BigDecimal.ZERO;
		cantidadBuy = 0;
		cantidadSell = 0;
	}


	public static void sumarTradeData(TradeData tradeIteracion) {
		if (tradeIteracion.getSide().equals("Buy")) {
			OrdenesHelper.setVolumenAcumuladoBuy(OrdenesHelper.getVolumenAcumuladoBuy().add(tradeIteracion.getSize()));
			OrdenesHelper.setPrecioVolumenAcumuladoBuy(OrdenesHelper.getPrecioVolumenAcumuladoBuy()
					.add(new BigDecimal(tradeIteracion.getPrice()).multiply(tradeIteracion.getSize())));
			OrdenesHelper.setCantidadBuy(OrdenesHelper.getCantidadBuy() + 1);
		} else {
			OrdenesHelper.setVolumenAcumuladoSell(OrdenesHelper.getVolumenAcumuladoSell().add(tradeIteracion.getSize()));
			OrdenesHelper.setPrecioVolumenAcumuladoSell(OrdenesHelper.getPrecioVolumenAcumuladoSell().add(new BigDecimal(tradeIteracion.getPrice()).multiply(tradeIteracion.getSize())));
			OrdenesHelper.setCantidadSell(OrdenesHelper.getCantidadSell() + 1);
		}
	}

	/**
	 * Retorna la media ponderada de las entradas en buy
	 * 
	 * @return
	 */
	public static BigDecimal calcularMediaPonderadaBuy() {
		if (volumenAcumuladoBuy != null && !volumenAcumuladoBuy.equals(BigDecimal.ZERO)) {
			return precioVolumenAcumuladoBuy.divide(volumenAcumuladoBuy, 1, RoundingMode.HALF_UP);
		}
		return BigDecimal.ZERO;
	}

	/**
	 * Retorna la media ponderada de las entradas en sell
	 * 
	 * @return
	 */
	public static BigDecimal calcularMediaPonderadaSell() {
		if (volumenAcumuladoSell != null && !volumenAcumuladoSell.equals(BigDecimal.ZERO)) {
			return precioVolumenAcumuladoSell.divide(volumenAcumuladoSell, 1, RoundingMode.HALF_UP);
		}
		return BigDecimal.ZERO;
	}


}

package com.scalpingjedi.recolector.helper;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "singleton")
public class FechaHelper {

	public static Long getTimeDeLocalDateTime(LocalDateTime fecha) {
		return Date.from(fecha.atZone(ZoneOffset.UTC).toInstant()).getTime();
	}
	
	public LocalDateTime dateToLocalDateTime(Date dateToConvert) {
	    return dateToConvert.toInstant().atZone(ZoneOffset.UTC).toLocalDateTime();
	}
}

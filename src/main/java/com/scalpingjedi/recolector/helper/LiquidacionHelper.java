package com.scalpingjedi.recolector.helper;



import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.scalpingjedi.comun.entidades.Liquidacion;

@Component
@Scope(value = "singleton")
public class LiquidacionHelper {
	
	
	private static Liquidacion liquidacionesActivas;

	public static synchronized Liquidacion getLiquidacionesActivas() {
		return liquidacionesActivas;
	}

	public static synchronized void setLiquidacionesActivas(Liquidacion liquidacionesActivas) {
		LiquidacionHelper.liquidacionesActivas = liquidacionesActivas;
	}
	
	
	
	private static BigDecimal volumenAcumuladoLong;
	private static BigDecimal precioVolumenAcumuladoLong;
	private static List<BigDecimal> precioLiquidacionLong;

	private static BigDecimal volumenAcumuladoShort;
	private static BigDecimal precioVolumenAcumuladoShort;
	private static List<BigDecimal> precioLiquidacionShort;
	
	
	
	public static void inicializarAcumuladores() {
		volumenAcumuladoLong = BigDecimal.ZERO;
		volumenAcumuladoShort = BigDecimal.ZERO;
		precioVolumenAcumuladoLong = BigDecimal.ZERO;
		precioVolumenAcumuladoShort = BigDecimal.ZERO;
		precioLiquidacionShort = new ArrayList<>();
		precioLiquidacionLong = new ArrayList<>();
	}
	
	public static synchronized BigDecimal getVolumenAcumuladoLong() {
		return volumenAcumuladoLong;
	}

	public static synchronized void setVolumenAcumuladoLong(BigDecimal volumenAcumuladoLong) {
		LiquidacionHelper.volumenAcumuladoLong = volumenAcumuladoLong;
	}

	public static synchronized BigDecimal getVolumenAcumuladoShort() {
		return volumenAcumuladoShort;
	}
	
	public static synchronized List<BigDecimal> getPrecioLiquidacionShort() {
		return precioLiquidacionShort;
	}
	
	public static synchronized void setPrecioLiquidacionShort(List<BigDecimal> precioLiquidacionShort) {
		LiquidacionHelper.precioLiquidacionShort = precioLiquidacionShort;
	}
	
	public static synchronized List<BigDecimal> getPrecioLiquidacionLong() {
		return precioLiquidacionLong;
	}
	
	public static synchronized void setPrecioLiquidacionLong(List<BigDecimal> precioLiquidacionLong) {
		LiquidacionHelper.precioLiquidacionLong = precioLiquidacionLong;
	}


	public static synchronized void setVolumenAcumuladoShort(BigDecimal volumenAcumuladoShort) {
		LiquidacionHelper.volumenAcumuladoShort = volumenAcumuladoShort;
	}

	public static synchronized BigDecimal getPrecioVolumenAcumuladoShort() {
		return precioVolumenAcumuladoShort;
	}

	public static synchronized void setPrecioVolumenAcumuladoShort(BigDecimal precioVolumenAcumuladoShort) {
		LiquidacionHelper.precioVolumenAcumuladoShort = precioVolumenAcumuladoShort;
	}

	public static synchronized BigDecimal getPrecioVolumenAcumuladoLong() {
		return precioVolumenAcumuladoLong;
	}

	public static synchronized void setPrecioVolumenAcumuladoLong(BigDecimal precioVolumenAcumuladoLong) {
		LiquidacionHelper.precioVolumenAcumuladoLong = precioVolumenAcumuladoLong;
	}
	
	
	
	public static void sumarLiquidaciones(BigDecimal precio, String side, BigDecimal size) {
		if (side.equals("Long")) {
			LiquidacionHelper.setVolumenAcumuladoLong(LiquidacionHelper.getVolumenAcumuladoLong().add(size));
			LiquidacionHelper.setPrecioVolumenAcumuladoLong(LiquidacionHelper.getPrecioVolumenAcumuladoLong().add(precio.multiply(size)));
		} else {
			LiquidacionHelper.setVolumenAcumuladoShort(LiquidacionHelper.getVolumenAcumuladoShort().add(size));
			LiquidacionHelper.setPrecioVolumenAcumuladoShort(LiquidacionHelper.getPrecioVolumenAcumuladoShort().add(precio.multiply(size)));
		}
	}


	/**
	 * Retorna la media ponderada de las entradas en Long
	 * 
	 * @return
	 */
	public static BigDecimal calcularMediaPonderadaLong() {
		if (volumenAcumuladoLong != null && !volumenAcumuladoLong.equals(BigDecimal.ZERO)) {
			return precioVolumenAcumuladoLong.divide(volumenAcumuladoLong, 1, RoundingMode.HALF_UP);
		}
		return BigDecimal.ZERO;
	}

	/**
	 * Retorna la media ponderada de las entradas en Short
	 * 
	 * @return
	 */
	public static BigDecimal calcularMediaPonderadaShort() {
		if (volumenAcumuladoShort != null && !volumenAcumuladoShort.equals(BigDecimal.ZERO)) {
			return precioVolumenAcumuladoShort.divide(volumenAcumuladoShort, 1, RoundingMode.HALF_UP);
		}
		return BigDecimal.ZERO;
	}
	


}

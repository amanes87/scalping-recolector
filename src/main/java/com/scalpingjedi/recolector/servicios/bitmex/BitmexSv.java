package com.scalpingjedi.recolector.servicios.bitmex;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.threeten.bp.OffsetDateTime;

import com.scalpingjedi.comun.entidades.VelaBitmex;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;

import io.swagger.client.ApiException;
import io.swagger.client.api.TradeApi;
import io.swagger.client.model.TradeBin;

/**
 * Servicio encargado de llamar a los WebServices de Bitmex
 * @author Adrià
 *
 */
@Component
public class BitmexSv {

	public List<VelaBitmex> getVelasPasadas(){
		TradeApi apiInstance = new TradeApi();
		try {
			OffsetDateTime fechaInicio = OffsetDateTime.now().minusDays(1L);
			OffsetDateTime fechaFin = OffsetDateTime.now().plusDays(1L);
			List<TradeBin> result = apiInstance.tradeGetBucketed("5m", Boolean.TRUE, "XBTUSD", "", "", new BigDecimal(100), BigDecimal.ZERO, Boolean.TRUE, fechaInicio, fechaFin);
			result.get(0);
			return result.stream().map(tb -> new VelaBitmex(tb)).collect(Collectors.toList());
		} catch (ApiException e) {
			System.err.println("Exception when calling AnnouncementApi#announcementGet");
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Vela_005> getVelasPasadas005(){
		List<VelaBitmex> velasPasadas = getVelasPasadas();
		return velasPasadas.stream().map(v -> new Vela_005(v)).collect(Collectors.toList());
	}
	
}

package com.scalpingjedi.recolector.servicios.bitmex.websockets;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.scalpingjedi.comun.entidades.Liquidacion;
import com.scalpingjedi.comun.entidades.dto.liquidation.LiquidationDTO;
import com.scalpingjedi.comun.entidades.dto.liquidation.LiquidationData;
import com.scalpingjedi.recolector.helper.DatosEnMemoria;
import com.scalpingjedi.recolector.helper.LiquidacionHelper;

@Component
public class WebSocketLiquidaciones {

	@Autowired
	private DatosEnMemoria datosEnMemoria;

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	private final String URL_LIQUIDACIONES = "wss://www.bitmex.com/realtime?subscribe=liquidation:XBTUSD";

	WebSocketClient webSocketClient = null;
	public WebSocketClient iniciar() {
		try {
			webSocketClient = new WebSocketClient(new URI(URL_LIQUIDACIONES)) {
				@Override
				public void onMessage(String message) {
					try {
						if (!message.contains("pong")) {
							Gson gson = new Gson();
							LiquidationDTO liquidationOnMessage = gson.fromJson(message, LiquidationDTO.class);
							if ("insert".equalsIgnoreCase(liquidationOnMessage.getAction())) {
								LiquidationData[] ld = liquidationOnMessage.getData();
								if (ld.length > 0) {
									for (int i = 0; i < ld.length; i++) {
										LiquidationData liquidationIteracion = ld[i];
										BigDecimal precioLiquidacion = new BigDecimal(liquidationIteracion.getPrice());
										if (liquidationIteracion.getSide().equalsIgnoreCase("sell") && liquidationIteracion.getLeavesQty() != null) {
											LiquidacionHelper.getLiquidacionesActivas().setLongLiquidation(true);
											LiquidacionHelper.getLiquidacionesActivas().setLongLiquidationQuantity(LiquidacionHelper.getLiquidacionesActivas().getLongLiquidationQuantity() + liquidationIteracion.getLeavesQty());
											LiquidacionHelper.sumarLiquidaciones(precioLiquidacion, "Long", new BigDecimal(liquidationIteracion.getLeavesQty()));
											List<BigDecimal> precioLiquidacionList = LiquidacionHelper.getPrecioLiquidacionLong();
											precioLiquidacionList.add(precioLiquidacion);
											LiquidacionHelper.setPrecioLiquidacionLong(precioLiquidacionList);
										} else if (liquidationIteracion.getSide().equalsIgnoreCase("buy") && liquidationIteracion.getLeavesQty() != null) {
											LiquidacionHelper.getLiquidacionesActivas().setShortLiquidation(true);
											LiquidacionHelper.getLiquidacionesActivas().setShortLiquidationQuantity(LiquidacionHelper.getLiquidacionesActivas().getShortLiquidationQuantity() + liquidationIteracion.getLeavesQty());
											LiquidacionHelper.sumarLiquidaciones(precioLiquidacion, "Short", new BigDecimal(liquidationIteracion.getLeavesQty()));
											List<BigDecimal> precioLiquidacionList = LiquidacionHelper.getPrecioLiquidacionLong();
											precioLiquidacionList.add(precioLiquidacion);
											LiquidacionHelper.setPrecioLiquidacionShort(precioLiquidacionList);
										}
									}
								}
							}
						}
					} catch (Exception e) {
						log.error("Error WS Liquidaciones Definido");
						log.error(e.getMessage());
						log.error(e.getStackTrace().toString());
					}
				}

				@Override
				public void onOpen(ServerHandshake handshake) {
					String mensaje = "Abierto WS Liquidaciones";
					log.info(mensaje);
					datosEnMemoria.getTelegramSv().notifyMessage(mensaje);
					LiquidacionHelper.inicializarAcumuladores();
					LiquidacionHelper.setLiquidacionesActivas(new Liquidacion());
					ejecutar(webSocketClient);
				}

				@Override
				public void onClose(int code, String reason, boolean remote) {
					log.error("Cerrado WS Liquidaciones");
					WebSocket socketLiquidaciones = iniciar();
					datosEnMemoria.setSocketLiquidaciones(socketLiquidaciones);
					String mensaje = reason;
					log.info(mensaje);
					datosEnMemoria.getTelegramSv().notifyMessage("WS Liquidaciones Close. " + mensaje);
				}

				@Override
				public void onError(Exception ex) {
					log.error("Error WS Liquidaciones");
					ex.printStackTrace();
					String errorMessage = ex.getMessage();
					log.error(errorMessage);
					datosEnMemoria.getTelegramSv().notifyMessage("WS Liquidaciones Error. " + errorMessage);
				}

			};
			webSocketClient.connect();
			return webSocketClient;
		} catch (URISyntaxException e) {
			String mensaje = "HA HABIDO UN ERROR CON LA CONEXION DE " + URL_LIQUIDACIONES;
			log.error(mensaje);
			datosEnMemoria.getTelegramSv().notifyMessage(mensaje);
		}
		return null;
	}
	
	void ejecutar(WebSocketClient wsLiquidaciones) {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					wsLiquidaciones.send("ping");
				} catch (Exception e) {
					log.info("Error en el hilo ping pong de liquidaciones");
					exec.shutdown();
				}
			}

		}, 0, 5, TimeUnit.SECONDS);
	}

}

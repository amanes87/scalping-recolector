package com.scalpingjedi.recolector.servicios.bitmex.websockets;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.sql.rowset.spi.SyncResolver;

import org.apache.commons.collections.CollectionUtils;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scalpingjedi.comun.entidades.Liquidacion;
import com.scalpingjedi.comun.entidades.bitfinex.candle.Candle;
import com.scalpingjedi.comun.entidades.dto.tradebin.TradeBinDTO;
import com.scalpingjedi.comun.entidades.dto.tradebin.TradeBinData;
import com.scalpingjedi.comun.entidades.kills.Kill;
import com.scalpingjedi.comun.entidades.pools.Pool;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;
import com.scalpingjedi.comun.servicios.Vela_005Repository;
import com.scalpingjedi.comun.servicios.kills.KillRepository;
import com.scalpingjedi.comun.servicios.pools.PoolRepository;
import com.scalpingjedi.recolector.helper.DatosEnMemoria;
import com.scalpingjedi.recolector.helper.LiquidacionHelper;
import com.scalpingjedi.recolector.helper.OrdenesHelper;
import com.scalpingjedi.recolector.helper.PoolsHelper;
import com.scalpingjedi.recolector.servicios.bitfinex.websocket.BitfinexWebSocket;

import lombok.val;

@Component
public class WebSocketVelas {

	private final String URL_VELAS = "wss://www.bitmex.com/realtime?subscribe=tradeBin5m:XBTUSD";

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	public BitfinexWebSocket bitfinexWebSocket;

	@Autowired
	public Vela_005Repository vela_005Repository;

	@Autowired
	public WebSocketTrades wsTrades;

	@Autowired
	private DatosEnMemoria datosEnMemoria;

	@Autowired
	public KillRepository killRepository;

	@Autowired
	public PoolRepository poolRepository;

	WebSocketClient webSocketClient = null;

	public WebSocketClient iniciar() {
		try {
			webSocketClient = new WebSocketClient(new URI(URL_VELAS)) {

				@Override
				public void onOpen(ServerHandshake handshakedata) {
					String mensaje = "Abierto WS Velas";
					log.info(mensaje);
					datosEnMemoria.getTelegramSv().notifyMessage(mensaje);
					bitfinexWebSocket.iniciar();
					ejecutar(webSocketClient);
				}

				@Override
				public void onMessage(String message) {
					try {
						if (!message.contains("pong") && message.contains("table") && message.contains("insert") && message.contains("XBTUSD")) {
							Gson gson = new Gson();
							TradeBinDTO velaBitmex = gson.fromJson(message, TradeBinDTO.class);
							TradeBinData[] td = velaBitmex.getData();
							if (td != null && td.length > 0) {
								TradeBinData tradeBinRecolector = td[0];
								if (LiquidacionHelper.getLiquidacionesActivas() != null && LiquidacionHelper.getLiquidacionesActivas().getLongLiquidation() != null && LiquidacionHelper.getLiquidacionesActivas().getLongLiquidation()) {
									List<Double> preciosLiquidacion = new ArrayList<>();
									for (BigDecimal precio : LiquidacionHelper.getPrecioLiquidacionLong()) {
										preciosLiquidacion.add(precio.doubleValue());
									}
									Double precioMedioLiquidacion = preciosLiquidacion.stream().mapToDouble(Double::doubleValue).average().orElse(0d);
									LiquidacionHelper.getLiquidacionesActivas().setPrecioMedioLongLiquidation(new BigDecimal(precioMedioLiquidacion));
								}
								if (LiquidacionHelper.getLiquidacionesActivas() != null && LiquidacionHelper.getLiquidacionesActivas().getShortLiquidation() != null && LiquidacionHelper.getLiquidacionesActivas().getShortLiquidation()) {
									List<Double> preciosLiquidacion = new ArrayList<>();
									for (BigDecimal precio : LiquidacionHelper.getPrecioLiquidacionShort()) {
										preciosLiquidacion.add(precio.doubleValue());
									}
									Double precioMedioLiquidacion = preciosLiquidacion.stream().mapToDouble(Double::doubleValue).average().orElse(0d);
									LiquidacionHelper.getLiquidacionesActivas().setPrecioMedioShortLiquidation(new BigDecimal(precioMedioLiquidacion));
								}
								Vela_005 vela = new Vela_005(tradeBinRecolector, LiquidacionHelper.getLiquidacionesActivas());
								// pillar vela de memoria y asignar.

								BigDecimal precioMedioBuy = OrdenesHelper.calcularMediaPonderadaBuy();
								vela.setPrecioMedioBuy(precioMedioBuy);
								BigDecimal precioMedioSell = OrdenesHelper.calcularMediaPonderadaSell();
								vela.setPrecioMedioSell(precioMedioSell);
								vela.setVolumenBuy(OrdenesHelper.getVolumenAcumuladoBuy());
								vela.setVolumenSell(OrdenesHelper.getVolumenAcumuladoSell());
								vela.setDineroAcumuladoBuy(OrdenesHelper.getPrecioVolumenAcumuladoBuy());
								vela.setDineroAcumuladoSell(OrdenesHelper.getPrecioVolumenAcumuladoSell());
								vela.setCantidadBuy(OrdenesHelper.getCantidadBuy());
								vela.setCantidadSell(OrdenesHelper.getCantidadSell());
								LiquidacionHelper.setLiquidacionesActivas(new Liquidacion());
								OrdenesHelper.inicializarAcumuladores();
								LiquidacionHelper.inicializarAcumuladores();
								wsTrades.guardarVelas(null, vela);
								tratarKills(vela, vela.getLongLiquidation(), vela.getShortLiquidation());
								// TRATAR POOLS(RESETEO, ETC)
								tratarYGuardarPools(vela);
								Vela_005 nuevaVela = Vela_005.generarVelaSiguiente(vela);
								OrdenesHelper.setVelaEnMemoria(nuevaVela);
							}
						}
					} catch (Exception e) {
						log.error("Error WS Velas Definido");
						log.error("StackTrace del error: ", e);
						log.error("Printando error WS Velas Definido");
						e.getStackTrace();
						datosEnMemoria.getTelegramSv().notifyMessage("Error WS VELAS DEFINIDO " + e);
						PoolsHelper.setPoolGap(Boolean.TRUE);
					}
				}

				@Override
				public void onClose(int code, String reason, boolean remote) {
					WebSocket socket = iniciar();
					datosEnMemoria.setSocketVelas(socket);
					String mensaje = reason;
					log.info(mensaje);
					datosEnMemoria.getTelegramSv().notifyMessage("WS Velas Close. " + mensaje);
				}

				@Override
				public void onError(Exception ex) {
					log.error("Error WS Velas");
					ex.printStackTrace();
					String errorMessage = ex.getMessage();
					log.error(errorMessage);
					datosEnMemoria.getTelegramSv().notifyMessage("WS Velas Error. " + errorMessage);
				}
			};
			webSocketClient.connect();
			return webSocketClient;
		} catch (URISyntaxException e) {
			String mensaje = "HA HABIDO UN ERROR CON LA CONEXION DE " + URL_VELAS;
			log.error(mensaje);
			datosEnMemoria.getTelegramSv().notifyMessage(mensaje);
		}
		return webSocketClient;
	}

	void ejecutar(WebSocketClient wsVelas) {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					wsVelas.send("ping");
				} catch (Exception e) {
					log.info("Error en el hilo ping pong de velas");
					exec.shutdown();
				}
			}

		}, 0, 5, TimeUnit.SECONDS);
	}

	protected synchronized void tratarYGuardarPools(Vela_005 vela) {
		if (PoolsHelper.getPoolGap().equals(Boolean.TRUE)) {
			log.info("Detectado pool gap");
			LocalDateTime horaCierreActual = vela.getHoraCierre();
			LocalDateTime horaCierreAnterior = horaCierreActual.minusMinutes(5);
			Vela_005 velaAnterior = vela_005Repository.findVelaDate(horaCierreAnterior);
			if (velaAnterior != null) {
				log.info("Vela anterior recuparada");
				List<Object[]> pools = poolRepository.obtenerPoolsAgrupadosPorHora(horaCierreAnterior);
				List<Object[]> poolsAnteriores = poolRepository.obtenerPoolsAgrupadosPorHora(horaCierreAnterior.minusMinutes(5));
				double cantidad = Double.valueOf(pools.size());
				double cantidadAnteriores = Double.valueOf(poolsAnteriores.size());
				if (CollectionUtils.isEmpty(pools) || cantidadAnteriores / 2 >= cantidad) {
					log.info("Rellenando gap Pool");
					tratarPools(velaAnterior);
					guardarPools(velaAnterior);
				}
			} else {
				log.info("Error falta pool y velaAnterior");
			}
			PoolsHelper.setPoolGap(Boolean.FALSE);
		}
		tratarPools(vela);
//		logarPools();
		guardarPools(vela);
	}

	private synchronized void logarPools() {
		log.info("+++++++++++++++++++++++++++++++++++++++++++");
		PoolsHelper.getMapPools100x().entrySet().stream().sorted((es1, es2) -> es1.getKey().compareTo(es2.getKey())).forEach(es -> {
			log.info("100x POOL {}", es.getKey());
		});
		log.info("============================================");
		PoolsHelper.getMapPools50x().entrySet().stream().sorted((es1, es2) -> es1.getKey().compareTo(es2.getKey())).forEach(es -> {
			log.info("50x POOL {}", es.getKey());
		});
		log.info("============================================");
		PoolsHelper.getMapPools25x().entrySet().stream().sorted((es1, es2) -> es1.getKey().compareTo(es2.getKey())).forEach(es -> {
			log.info("25x POOL {}", es.getKey());
		});
		log.info("============================================");
		PoolsHelper.getMapPools10x().entrySet().stream().sorted((es1, es2) -> es1.getKey().compareTo(es2.getKey())).forEach(es -> {
			log.info("10x POOL {}", es.getKey());
		});
		log.info("============================================");
		PoolsHelper.getMapPools5x().entrySet().stream().sorted((es1, es2) -> es1.getKey().compareTo(es2.getKey())).forEach(es -> {
			log.info("5x POOL {}", es.getKey());
		});
		log.info("+++++++++++++++++++++++++++++++++++++++++++");
	}

	public static HashMap<Integer, BigDecimal> clonar(Map<Integer, BigDecimal> original) {
		Gson gson = new Gson();
		String jsonString = gson.toJson(original);
		Type type = new TypeToken<HashMap<Integer, BigDecimal>>(){}.getType();
		HashMap<Integer, BigDecimal> clonedMap = gson.fromJson(jsonString, type);
		return clonedMap;
	}

	protected synchronized void guardarPools(Vela_005 vela) {
		LocalDateTime horaVela = vela.getHoraCierre();
		PoolsHelper.setId2(PoolsHelper.getId2() + 1);
		int id2 = PoolsHelper.getId2();

		Map<Integer, BigDecimal> pools100x = new HashMap<>();
		Map<Integer, BigDecimal> pools50x = new HashMap<>();
		Map<Integer, BigDecimal> pools25x = new HashMap<>();
		Map<Integer, BigDecimal> pools10x = new HashMap<>();
		Map<Integer, BigDecimal> pools5x = new HashMap<>();
		if (!PoolsHelper.getMapPools100x().isEmpty()) {
			pools100x =clonar(PoolsHelper.getMapPools100x());
		}
		if (!PoolsHelper.getMapPools50x().isEmpty()) {
			pools50x = clonar(PoolsHelper.getMapPools50x());
		}
		if (!PoolsHelper.getMapPools25x().isEmpty()) {
			pools25x = clonar(PoolsHelper.getMapPools25x());
		}
		if (!PoolsHelper.getMapPools10x().isEmpty()) {
			pools10x = clonar(PoolsHelper.getMapPools10x());
		}
		if (!PoolsHelper.getMapPools5x().isEmpty()) {
			pools5x = clonar(PoolsHelper.getMapPools5x());
		}
		for (Entry<Integer, BigDecimal> valorMap : pools100x.entrySet()) {
			Pool pool = new Pool(horaVela, valorMap.getKey(), valorMap.getValue(), "100x", id2);
			poolRepository.save(pool);
		}
		for (Entry<Integer, BigDecimal> valorMap : pools50x.entrySet()) {
			Pool pool = new Pool(horaVela, valorMap.getKey(), valorMap.getValue(), "50x", id2);
			poolRepository.save(pool);
		}
		for (Entry<Integer, BigDecimal> valorMap : pools25x.entrySet()) {
			Pool pool = new Pool(horaVela, valorMap.getKey(), valorMap.getValue(), "25x", id2);
			poolRepository.save(pool);
		}
		for (Entry<Integer, BigDecimal> valorMap : pools10x.entrySet()) {
			Pool pool = new Pool(horaVela, valorMap.getKey(), valorMap.getValue(), "10x", id2);
			poolRepository.save(pool);
		}
		for (Entry<Integer, BigDecimal> valorMap : pools5x.entrySet()) {
			Pool pool = new Pool(horaVela, valorMap.getKey(), valorMap.getValue(), "5x", id2);
			poolRepository.save(pool);
		}
	}

	public void inicializarPools() {
		log.info("Inicializando los pools");
		PoolsHelper.inicializarPools();
		List<Pool> ultimosPools = poolRepository.obtenerUltimosPools();
		for (Pool pool : ultimosPools) {
			if ("100x".equalsIgnoreCase(pool.getApalancamiento())) {
				PoolsHelper.getMapPools100x().put(pool.getPrecio(), pool.getVolumen());
			} else if ("50x".equalsIgnoreCase(pool.getApalancamiento())) {
				PoolsHelper.getMapPools50x().put(pool.getPrecio(), pool.getVolumen());
			} else if ("25x".equalsIgnoreCase(pool.getApalancamiento())) {
				PoolsHelper.getMapPools25x().put(pool.getPrecio(), pool.getVolumen());
			} else if ("10x".equalsIgnoreCase(pool.getApalancamiento())) {
				PoolsHelper.getMapPools10x().put(pool.getPrecio(), pool.getVolumen());
			} else if ("5x".equalsIgnoreCase(pool.getApalancamiento())) {
				PoolsHelper.getMapPools5x().put(pool.getPrecio(), pool.getVolumen());
			}
		}
	}

	private synchronized void tratarPools(Vela_005 vela) {
		Double maximo = vela.getHigh().doubleValue();
		Double minimo = vela.getLow().doubleValue();
		Double cierre = vela.getClose().doubleValue();
		List<Integer> preciosAgrupados = obtenerPreciosComprendidos(minimo, maximo);
		for (Integer precio : preciosAgrupados) {
			if (!PoolsHelper.getMapPools100x().isEmpty() && PoolsHelper.getMapPools100x().containsKey(precio)) {
//				log.info("Elimino 100x -> " + precio);
				PoolsHelper.getMapPools100x().remove(precio);
			}
			if (!PoolsHelper.getMapPools50x().isEmpty() && PoolsHelper.getMapPools50x().containsKey(precio)) {
//				log.info("Elimino 50x -> " + precio);
				PoolsHelper.getMapPools50x().remove(precio);
			}
			if (!PoolsHelper.getMapPools25x().isEmpty() && PoolsHelper.getMapPools25x().containsKey(precio)) {
//				log.info("Elimino 25x -> " + precio);
				PoolsHelper.getMapPools25x().remove(precio);
			}
			if (!PoolsHelper.getMapPools10x().isEmpty() && PoolsHelper.getMapPools10x().containsKey(precio)) {
//				log.info("Elimino 10x -> " + precio);
				PoolsHelper.getMapPools10x().remove(precio);
			}
			if (!PoolsHelper.getMapPools5x().isEmpty() && PoolsHelper.getMapPools5x().containsKey(precio)) {
//				log.info("Elimino 5x -> " + precio);
				PoolsHelper.getMapPools5x().remove(precio);
			}
		}

		// eliminamos pools si el precio se aleja un 25% del pool
		Map<Integer, BigDecimal> pools100x = new HashMap<>();
		Map<Integer, BigDecimal> pools50x = new HashMap<>();
		Map<Integer, BigDecimal> pools25x = new HashMap<>();
		Map<Integer, BigDecimal> pools10x = new HashMap<>();
		Map<Integer, BigDecimal> pools5x = new HashMap<>();
		if (!PoolsHelper.getMapPools100x().isEmpty()) {
			pools100x =clonar(PoolsHelper.getMapPools100x());
		}
		if (!PoolsHelper.getMapPools50x().isEmpty()) {
			pools50x = clonar(PoolsHelper.getMapPools50x());
		}
		if (!PoolsHelper.getMapPools25x().isEmpty()) {
			pools25x = clonar(PoolsHelper.getMapPools25x());
		}
		if (!PoolsHelper.getMapPools10x().isEmpty()) {
			pools10x = clonar(PoolsHelper.getMapPools10x());
		}
		if (!PoolsHelper.getMapPools5x().isEmpty()) {
			pools5x = clonar(PoolsHelper.getMapPools5x());
		}
		for (Entry<Integer, BigDecimal> valorMap : pools100x.entrySet()) {
			if (cierre.compareTo(valorMap.getKey().doubleValue()) > 0) {
				// pool inferior(longs)
				double pool = valorMap.getKey();
				double porcentaje100 = (cierre * 100) / pool;
				double porcentaje = porcentaje100 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools100x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools100x().remove(valorMap.getKey());
				}
			} else {
				// pool superior(shorts)
				double pool = valorMap.getKey().doubleValue();
				double porcentaje100 = (pool * 100) / cierre;
				double porcentaje = porcentaje100 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools100x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools100x().remove(valorMap.getKey());
				}
			}
		}
		for (Entry<Integer, BigDecimal> valorMap : pools50x.entrySet()) {
			if (cierre.compareTo(valorMap.getKey().doubleValue()) > 0) {
				// pool inferior(longs)
				double pool = valorMap.getKey();
				double porcentaje50 = (cierre * 100) / pool;
				double porcentaje = porcentaje50 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools50x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools50x().remove(valorMap.getKey());
				}
			} else {
				// pool superior(shorts)
				double pool = valorMap.getKey();
				double porcentaje50 = (pool * 100) / cierre;
				double porcentaje = porcentaje50 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools50x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools50x().remove(valorMap.getKey());

				}
			}
		}
		for (Entry<Integer, BigDecimal> valorMap : pools25x.entrySet()) {
			if (cierre.compareTo(valorMap.getKey().doubleValue()) > 0) {
				// pool inferior(longs)
				double pool = valorMap.getKey().doubleValue();
				double porcentaje25 = (cierre.doubleValue() * 100) / pool;
				double porcentaje = porcentaje25 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools25x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools25x().remove(valorMap.getKey());

				}
			} else {
				// pool superior(shorts)
				double pool = valorMap.getKey().doubleValue();
				double porcentaje25 = (pool * 100) / cierre.doubleValue();
				double porcentaje = porcentaje25 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools25x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools25x().remove(valorMap.getKey());
				}
			}
		}
		for (Entry<Integer, BigDecimal> valorMap : pools10x.entrySet()) {
			if (cierre.compareTo(valorMap.getKey().doubleValue()) > 0) {
				// pool inferior(longs)
				double pool = valorMap.getKey().doubleValue();
				double porcentaje25 = (cierre.doubleValue() * 100) / pool;
				double porcentaje = porcentaje25 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools10x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools10x().remove(valorMap.getKey());

				}
			} else {
				// pool superior(shorts)
				double pool = valorMap.getKey().doubleValue();
				double porcentaje25 = (pool * 100) / cierre.doubleValue();
				double porcentaje = porcentaje25 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools10x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools10x().remove(valorMap.getKey());
				}
			}
		}
		for (Entry<Integer, BigDecimal> valorMap : pools5x.entrySet()) {
			if (cierre.compareTo(valorMap.getKey().doubleValue()) > 0) {
				// pool inferior(longs)
				double pool = valorMap.getKey().doubleValue();
				double porcentaje25 = (cierre.doubleValue() * 100) / pool;
				double porcentaje = porcentaje25 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools5x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools5x().remove(valorMap.getKey());

				}
			} else {
				// pool superior(shorts)
				double pool = valorMap.getKey().doubleValue();
				double porcentaje25 = (pool * 100) / cierre.doubleValue();
				double porcentaje = porcentaje25 - 100;
				if (porcentaje >= 25 && PoolsHelper.getMapPools5x().containsKey(valorMap.getKey())) {
					PoolsHelper.getMapPools5x().remove(valorMap.getKey());
				}
			}
		}
	}

	private List<Integer> obtenerPreciosComprendidos(Double minimo, Double maximo) {
		List<Integer> preciosAgrupados = new ArrayList<>();
		Integer precio = PoolsHelper.retornarNumeroAgrupado(minimo);
		Integer precioLimite = PoolsHelper.retornarNumeroAgrupado(maximo);
		Integer incremento = 5;
		preciosAgrupados.add(precio);
		while (precio.compareTo(precioLimite) <= 0) {
			precio = precio + incremento;
			preciosAgrupados.add(precio);
		}
		return preciosAgrupados;
	}

	private void tratarKills(Vela_005 velaBitmex, boolean longLiquidation, boolean shortLiquidation) {
		Double precioBitmex = velaBitmex.getClose().doubleValue();
		if (OrdenesHelper.getLastCandle() != null) {
			Double precioBitfinex = OrdenesHelper.getLastCandle().getClose();
			// logica kills
			Candle velaBitfinex = OrdenesHelper.getLastCandle();
			Double velaPorArribaBitmex = null;
			Double velaPorArribaBitfinex = null;
			Double velaPorAbajoBitmex = null;
			Double velaPorAbajoBitfinex = null;
			// mecha-arriba
			if (velaBitmex.getClose().doubleValue() > velaBitmex.getOpen().doubleValue()) {
				velaPorArribaBitmex = (velaBitmex.getHigh().doubleValue() * 100) / velaBitmex.getClose().doubleValue();
			} else {
				velaPorArribaBitmex = (velaBitmex.getHigh().doubleValue() * 100) / velaBitmex.getOpen().doubleValue();
			}
			if (velaBitfinex.getClose() > velaBitfinex.getOpen()) {
				velaPorArribaBitfinex = (velaBitfinex.getHigh() * 100) / velaBitfinex.getClose();
			} else {
				velaPorArribaBitfinex = (velaBitfinex.getHigh() * 100) / velaBitfinex.getOpen();
			}
			// mecha-abajo
			if (velaBitmex.getClose().doubleValue() > velaBitmex.getOpen().doubleValue()) {
				velaPorAbajoBitmex = (velaBitmex.getOpen().doubleValue() * 100) / velaBitmex.getLow().doubleValue();
			} else {
				velaPorAbajoBitmex = (velaBitmex.getClose().doubleValue() * 100) / velaBitmex.getLow().doubleValue();
			}
			if (velaBitfinex.getClose() > velaBitfinex.getOpen()) {
				velaPorAbajoBitfinex = (velaBitfinex.getOpen() * 100) / velaBitfinex.getLow();
			} else {
				velaPorAbajoBitfinex = (velaBitfinex.getClose() * 100) / velaBitfinex.getLow();
			}
			boolean superaVelasPorArriba = velaPorArribaBitfinex < velaPorArribaBitmex;
			boolean superaVelasPorAbajo = velaPorAbajoBitfinex < velaPorAbajoBitmex;

			double limite = 100.25;
			if (superaVelasPorArriba && velaPorArribaBitmex > limite && shortLiquidation == Boolean.TRUE) {
				Kill kill = new Kill(velaBitmex.getHoraCierre(), velaPorArribaBitmex - 100d, "Short");
				killRepository.save(kill);
			}
			if (superaVelasPorAbajo && velaPorAbajoBitmex > limite && longLiquidation == Boolean.TRUE) {
				Kill kill = new Kill(velaBitmex.getHoraCierre(), velaPorAbajoBitmex - 100d, "Long");
				killRepository.save(kill);
			}

			log.info("PRECIO>>> BITMEX {} | BITFINEX: {}", precioBitmex, precioBitfinex);
		}
	}

}

package com.scalpingjedi.recolector.servicios.bitmex.websockets;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.scalpingjedi.comun.entidades.Orden;
import com.scalpingjedi.comun.entidades.dto.trade.TradeDTO;
import com.scalpingjedi.comun.entidades.dto.trade.TradeData;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;
import com.scalpingjedi.comun.entidades.trades.BigOrder;
import com.scalpingjedi.comun.entidades.trades.ResumenBigOrder;
import com.scalpingjedi.comun.entidades.trades.ResumenTrade;
import com.scalpingjedi.comun.servicios.Vela_005Repository;
import com.scalpingjedi.comun.servicios.pools.PoolRepository;
import com.scalpingjedi.comun.servicios.trades.BigOrderRepository;
import com.scalpingjedi.comun.servicios.trades.ResumenBigOrderRepository;
import com.scalpingjedi.comun.servicios.trades.ResumenTradeRepository;
import com.scalpingjedi.recolector.entidades.Trades;
import com.scalpingjedi.recolector.helper.DatosEnMemoria;
import com.scalpingjedi.recolector.helper.LiquidacionHelper;
import com.scalpingjedi.recolector.helper.OrdenesHelper;
import com.scalpingjedi.recolector.helper.PoolsHelper;

@Component
public class WebSocketTrades {

	private final String URL_TRADES = "wss://www.bitmex.com/realtime?subscribe=trade:XBTUSD";

	@Autowired
	private DatosEnMemoria datosEnMemoria;

	@Autowired
	public Vela_005Repository vela_005Repository;

	@Autowired
	public ResumenTradeRepository tradeRepository;

	@Autowired
	public ResumenBigOrderRepository resumenBigOrderRepository;

	@Autowired
	public BigOrderRepository bigOrderRepository;

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	WebSocketClient webSocketClient = null;
	
	public WebSocketClient iniciar() {
		try {
			webSocketClient = new WebSocketClient(new URI(URL_TRADES)) {

				@Override
				public void onOpen(ServerHandshake handshakedata) {
					String mensaje = "Abierto WS Trades";
					log.info(mensaje);
					datosEnMemoria.getTelegramSv().notifyMessage(mensaje);
					OrdenesHelper.setOrdenesAcumuladas(new Orden());
					OrdenesHelper.inicializarAcumuladores();
					ejecutar(webSocketClient);
				}

				@Override
				public void onMessage(String message) {
					// TODO FALTA REGISTRAR TODAS LAS BIGORDERS
					try {
 						if (!message.contains("pong")) {
							Gson gson = new Gson();
							TradeDTO tradeOnMessage = gson.fromJson(message, TradeDTO.class);
							TradeData[] td = tradeOnMessage.getData();
							if (td != null && td.length > 0) {
								tratamientoResumenTradesYBigOrders(td);
								// acumular shorts y longs
								tratamientoOrdenesVolumen(td);
							}
						}
					} catch (Exception e) {
						log.error("Error WS Trades Definido");
						log.error(e.getMessage());
						log.error(e.getStackTrace().toString());
					}
				}

				@Override
				public void onClose(int code, String reason, boolean remote) {
					log.info("Cerrado WS Trades");
					WebSocket socket = iniciar();
					datosEnMemoria.setSocketTrades(socket);
					String mensaje = reason;
					log.info(mensaje);
					datosEnMemoria.getTelegramSv().notifyMessage("WS Trades Close. " + mensaje);
				}

				@Override
				public void onError(Exception ex) {
					log.error("Error WS Trades");
					ex.printStackTrace();
					String errorMessage = ex.getMessage();
					log.error(errorMessage);
					datosEnMemoria.getTelegramSv().notifyMessage("WS Trades Error. " + errorMessage);
				}
			};
			webSocketClient.connect();
			return webSocketClient;
		} catch (URISyntaxException e) {
			String mensaje = "HA HABIDO UN ERROR CON LA CONEXION DE " + URL_TRADES;
			log.error(mensaje);
			datosEnMemoria.getTelegramSv().notifyMessage(mensaje);
		}
		return webSocketClient;
	}

	String minutoCierre = "5";
	boolean tradeInicializado = false;
	Trades trades;
	ResumenBigOrder resumenBigOrder;
	boolean cierreTrade = false;
	
	void ejecutar(WebSocketClient wsTrades) {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					wsTrades.send("ping");
				} catch (Exception e) {
					log.info("Error en el hilo ping pong de trades");
					exec.shutdown();
				}
			}

		}, 0, 5, TimeUnit.SECONDS);
	}

	private void tratamientoResumenTradesYBigOrders(TradeData[] td) {
		TradeData trade = td[0];
		String minutos = StringUtils.substringBeforeLast(trade.getTimestamp(), ":");
		if (minutos.endsWith(minutoCierre)) {
			// CIERRE VELA
			minutoCierre = minutoCierre.equals("5") ? "0" : "5";
			if (tradeInicializado) {
				insertarTrade(trades.obtenerResumenTrades());
				insertarResumenBigOrder(resumenBigOrder);
			}
			if (trade.getSize().compareTo(new BigDecimal(5000)) > 0) { // SI SON DE VOLUMEN > 5.000
				insertarBigTrades(trade);
			}
			trades = new Trades(trade);
			resumenBigOrder = new ResumenBigOrder(trade);

			cierreTrade = true;
			tradeInicializado = true;
		}
		if (!minutos.endsWith(minutoCierre) && tradeInicializado) {
			int indiceInicial = 0;
			if (cierreTrade) {
				indiceInicial = 1;
				cierreTrade = false;
			}
			// ACUMULAMOS
			for (int i = indiceInicial; i < td.length; i++) {
				TradeData tradeIteracion = td[i];
				// TODO Cambiar a enum
				if (trade.getSide().equals("Buy")) {
					trades.addBuy(tradeIteracion);
				} else {
					trades.addSell(tradeIteracion);
				}
				resumenBigOrder.acumularDatos(tradeIteracion);
				if (tradeIteracion.getSize().compareTo(new BigDecimal(5000)) > 0) { // SI SON DE VOLUMEN > 5.000
					insertarBigTrades(tradeIteracion);
				}
			}
		}
		if (resumenBigOrder != null) {
			insertarResumenBigOrder(resumenBigOrder);
		}
	}

	/**
	 * Hacemos el tratamiento para acumular el volumen
	 * 
	 * @param td
	 */
	private void tratamientoOrdenesVolumen(TradeData[] td) {
		for (int i = 0; i < td.length; i++) {
			TradeData tradeIteracion = td[i];
			OrdenesHelper.sumarTradeData(tradeIteracion);
			if (tradeIteracion.getSize().compareTo(new BigDecimal(15000)) > 0) {
				PoolsHelper.acumularPools(tradeIteracion);
			}
		}
	}

	/**
	 * Guardamos por cada trade la info en todo momento
	 */
//	protected void tratamientoVelaEnMemoria(TradeData[] td) {
//		if (OrdenesHelper.getVelaEnMemoria() != null) {
//			for (int i = 0; i < td.length; i++) {
//				TradeData tradeIteracion = td[i];
//				OrdenesHelper.getVelaEnMemoria().acumularTrade(tradeIteracion);
//			}
//			vela_005Repository.save(OrdenesHelper.getVelaEnMemoria());
//		}
//	}

	protected synchronized void guardarVelas(TradeData[] td, Vela_005 vela) {
		if (td != null) {
			if (OrdenesHelper.getVelaEnMemoria() != null) {
				for (int i = 0; i < td.length; i++) {
					TradeData tradeIteracion = td[i];
					OrdenesHelper.getVelaEnMemoria().acumularTrade(tradeIteracion);
				}
//				vela_005Repository.save(OrdenesHelper.getVelaEnMemoria());
			}
		} else if (vela != null) {
			vela_005Repository.save(vela);
		}
	}

	protected void insertarTrade(ResumenTrade trade) {
		tradeRepository.save(trade);
	}

	private void insertarResumenBigOrder(ResumenBigOrder resumen) {
		resumenBigOrderRepository.save(resumen);

	}

	protected void insertarBigTrades(TradeData trade) {
		bigOrderRepository.save(new BigOrder(trade));
	}
}

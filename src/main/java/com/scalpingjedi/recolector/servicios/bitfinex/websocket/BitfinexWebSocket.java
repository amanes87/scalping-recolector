package com.scalpingjedi.recolector.servicios.bitfinex.websocket;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.NotYetConnectedException;

import org.apache.commons.lang3.StringUtils;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.scalpingjedi.comun.entidades.bitfinex.candle.Candle;
import com.scalpingjedi.comun.servicios.bitfinex.candle.CandleRepository;
import com.scalpingjedi.recolector.helper.DatosEnMemoria;
import com.scalpingjedi.recolector.helper.OrdenesHelper;

@Component
public class BitfinexWebSocket {

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CandleRepository candleRepository;

	public WebSocketClient iniciar() {
		WebSocketClient webSocketClient = null;
		String uri = "wss://api-pub.bitfinex.com/ws/2";
		try {
			webSocketClient = new WebSocketClient(new URI(uri)) {

				@Override
				public void onOpen(ServerHandshake handshakedata) {
					// TODO Auto-generated method stub
					log.info("WEBSOCKET ABIERTO");
					String request = "{ \"event\": \"subscribe\", \"channel\": \"candles\", \"key\": \"trade:5m:tBTCUSD\" }";
					send(request);
				}

				@Override
				public void onMessage(String message) {
					// TODO Auto-generated method stub
					if (message.contains("[") && !message.contains("\"hb\"")) {
						convertirCandles(message);
					}
				}

				@Override
				public void onClose(int code, String reason, boolean remote) {
					// TODO Auto-generated method stub
					log.info("WEBSOCKET CLOSE BITFINEX!!!!!!!!!!");
						iniciar();
				}

				@Override
				public void onError(Exception ex) {
					// TODO Auto-generated method stub
					log.info("WEBSOCKET ERROR {}", ex);
					iniciar();
				}

				@Override
				public void send(String text) throws NotYetConnectedException {
					// TODO Auto-generated method stub
					log.info("MENSAJE ENVIADO: {}", text);
					super.send(text);
				}
			};
			webSocketClient.connect();
			return webSocketClient;
		} catch (URISyntaxException e) {
			log.info("HA HABIDO UN ERROR CON LA CONEXION DE {}", this.getClass().getName());
		}
		return null;
	}

	private void convertirCandles(String message) {
		message = StringUtils.substringAfter(message, ",[");
		message = StringUtils.substringBeforeLast(message, "]");
		message = StringUtils.remove(message, "]");
		String[] valores = StringUtils.split(message, "[");
		for (int i = 0; i < valores.length; i++) {
			String valor = valores[i];
			String[] datos = valor.split(",");
			Candle candle = new Candle(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5]);
			if (i == 0) {
				OrdenesHelper.setLastCandle(candle);
			}
			//candleRepository.save(candle);
		}
	}

}

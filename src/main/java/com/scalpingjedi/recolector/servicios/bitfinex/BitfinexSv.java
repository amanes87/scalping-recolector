package com.scalpingjedi.recolector.servicios.bitfinex;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.scalpingjedi.comun.entidades.bitfinex.candle.Candle;

/**
 * Servicio para llamar a los servicios rest de Bitfinex
 * @author Adrià
 *
 */
@Component
public class BitfinexSv {

	/**
	 * Método para obtener las velas pasadas de Bitfinex
	 * @param temporalidad
	 * @param tiempoInicio
	 * @param tiempoFin
	 * @return
	 */
	// https://api.bitfinex.com/v2/candles/trade:1h:tBTCUSD/hist?start=1514764800000&sort=1
	public List<Candle> getVelas(String temporalidad, String tiempoInicio, String tiempoFin) {
		String uri = "https://api.bitfinex.com/v2/candles/trade:" + temporalidad + ":tBTCUSD/hist";
		if (tiempoInicio != null) {
			uri = "https://api.bitfinex.com/v2/candles/trade:" + temporalidad + ":tBTCUSD/hist?start=" + tiempoInicio + "";
		}
		if (tiempoFin != null && tiempoInicio != null) {
			uri = "https://api.bitfinex.com/v2/candles/trade:" + temporalidad + ":tBTCUSD/hist?start=" + tiempoInicio + "?end=" + tiempoFin + "";
		}
		RestTemplate restTemplate = new RestTemplate();
		String resultado = restTemplate.getForObject(uri, String.class);
		JSONArray object = new JSONArray(resultado);
		List<Candle> velasPasadas = new ArrayList<>();
		for (int i = 0; i < object.length(); i++) {
			Instant msc = Instant.ofEpochMilli(object.getJSONArray(i).getLong(0));
			Double open = object.getJSONArray(i).getDouble(1);
			Double close = object.getJSONArray(i).getDouble(2);
			Double high = object.getJSONArray(i).getDouble(3);
			Double low = object.getJSONArray(i).getDouble(4);
			Double volume = object.getJSONArray(i).getDouble(5);
			Candle vbf = new Candle(msc, open, close, high, low, volume);
			velasPasadas.add(vbf);
		}
		return velasPasadas;
	}

}

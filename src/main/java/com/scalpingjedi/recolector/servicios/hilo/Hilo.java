package com.scalpingjedi.recolector.servicios.hilo;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.transaction.annotation.Transactional;

public class Hilo {

	@Transactional
	void ejecutar(String... args) {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		long delay = 0;
		if (args.length > 0) {
			delay = ChronoUnit.MILLIS.between(LocalTime.now(),
					LocalTime.of(Integer.valueOf(args[0]), Integer.valueOf(args[1]), 0));
		}
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					
				}catch(Exception e) {
					
				}
			}
		}, delay, delay, null);
	}
}

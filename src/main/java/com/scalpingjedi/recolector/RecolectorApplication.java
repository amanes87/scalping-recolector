package com.scalpingjedi.recolector;

import java.text.MessageFormat;

import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.scalpingjedi.recolector.helper.DatosEnMemoria;
import com.scalpingjedi.recolector.servicios.bitfinex.websocket.BitfinexWebSocket;
import com.scalpingjedi.recolector.servicios.bitmex.BitmexSv;
import com.scalpingjedi.recolector.servicios.bitmex.websockets.WebSocketLiquidaciones;
import com.scalpingjedi.recolector.servicios.bitmex.websockets.WebSocketTrades;
import com.scalpingjedi.recolector.servicios.bitmex.websockets.WebSocketVelas;

@SpringBootApplication
@ComponentScan({"com.scalpingjedi"})
@EntityScan("com.scalpingjedi")
@EnableJpaRepositories("com.scalpingjedi")
@PropertySource("classpath:aplicacion.properties")
public class RecolectorApplication implements CommandLineRunner{

	@Autowired
	public WebSocketTrades wsTrades;
	
	@Autowired
	public BitfinexWebSocket bitfinexWebSocket;
	
	@Autowired
	public WebSocketLiquidaciones wsLiquidaciones;
	
	@Autowired
	public WebSocketVelas wsVelas;
	
	@Autowired
	public BitmexSv bitmexSv;
	
	@Autowired
	public DatosEnMemoria datosEnMemoria;
	
	@Autowired
	private SecurityProperties configuration;
	
	@Value("${telegram.chatId}")
	private String chatId;
	
	@Value("${telegram.url}")
	private String url;
	
	@Value("${spring.profiles.active}")
	private String entorno;
	
	public static void main(String[] args) {
		SpringApplication.run(RecolectorApplication.class, args);
	}
	
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void run(String... args) throws Exception {
		
		datosEnMemoria.inicializarTelegram(chatId, url);
		
		//RELLENAR DATOS PASADOS
//		vela_005Repository.saveAll(bitmexSv.getVelasPasadas005()); //TODO MIRAR SI NO MACHACAMOS LOS DATOS PASADOS CALCULADOS EN EL WEBSOCKET
//		temporalidadesSv.generarRestoVelas();
		
		datosEnMemoria.getTelegramSv().notifyMessage(MessageFormat.format("#RECOLECTOR ({0}) PASSWORD:{1} | USUARIO:{2}", entorno, configuration.getUser().getPassword(), configuration.getUser().getName()));
		
		
		//Inicializar pools
		wsVelas.inicializarPools();
		
		//LEVANTAR WEBSOCKET de BITMEX
		WebSocket socketTradesBitmex = wsTrades.iniciar();
		datosEnMemoria.setSocketTrades(socketTradesBitmex);
		
		//LEVANTAR WEBSOCKET de BITMEX
		WebSocket socketVelas5Bitmex = wsVelas.iniciar();
		datosEnMemoria.setSocketVelas(socketVelas5Bitmex);
		
		//LEVANTAR WEBSOCKET de LIQUIDACIONES --> liquidation
		WebSocket socketLiquidaciones = wsLiquidaciones.iniciar();
		datosEnMemoria.setSocketLiquidaciones(socketLiquidaciones);
	}
	

	
	



}

package com.scalpingjedi.recolector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.telegram.service.TelegramSv;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ApplicationContext.class})
@EnableConfigurationProperties
public class RecolectorApplicationTests {

	@Autowired
	private ApplicationContext context;
	
	String chatId;
	
	String url;
	
	TelegramSv telegram;

	@Before
	public void establecerParametorsIniciales() {
		chatId = "-1001214988825";
		url = "https://api.telegram.org/bot553376516:AAE-4ig3LzcW4Pq-ZIeqCprydXUfYOojIss";
		telegram = new TelegramSv(chatId, url);
	}
	
	@Test
	public void testNotificacionMensaje() {
		telegram.notifyMessage("MENSAJE TEST NOTIFICACION MENSAJE");
	}
	
	@Test
	public void testNotificacionPhoto() {
		telegram.notifyPhoto("https://www.volumechart.com/wp-content/uploads/2018/12/cropped-Color-logo-no-background.png");
	}
	
	
	@Test
	public void testNotificacionMensajeYFoto() {
		telegram.notifyMessageAndPhoto("MENSAJE TEST DE NOTIFICACION DE MENSAJE Y FOTO", "https://www.volumechart.com/wp-content/uploads/2018/12/cropped-Color-logo-no-background.png");
	}
}
